﻿using Cusoft.Models;
using Cusoft.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cusoft
{
    /// <summary>
    /// Interaction logic for main_fin_state_page.xaml
    /// </summary>
    public partial class main_fin_state_page : Page
    {
        public main_fin_state_page()
        {
            InitializeComponent();

            ReportsService report = new ReportsService();
            
            List<fin_state> fin_states = new List<fin_state>();
            
            fin_states.Add(new fin_state() { Transaction = "TOTAL MEMBERSHIP ACCOUNT DEPOSIT AMOUNT", TotalAmount = report.GetTotalDeposits() });
            fin_states.Add(new fin_state() { Transaction = "TOTAL MEMBERSHIP ACCOUNT WITHDRAWAL AMOUNT", TotalAmount = report.GetTotalWithdrawals() });
            fin_states.Add(new fin_state() { Transaction = "TOTAL LOANS PROCESSED AMOUNT", TotalAmount = report.GetTotalLoans() });
            fin_states.Add(new fin_state() { Transaction = "TOTAL ACCOUNT BALANCE AMOUNT", TotalAmount = report.GetTotalAccountBalance() });

            fin_states.Add(new fin_state() { DataField = "ACCOUNT WITH HIGHEST SHARE PERCENTAGE", Data =report.GetAccountWithHighestSharePercentage().ToString() });
            fin_states.Add(new fin_state() { DataField = "ACCOUNT WITH LOWEST SHARE PERCENTAGE", Data = report.GetAccountWithLowestSharePercentage().ToString() });

            //datagrid1.ItemsSource = fin_states;
            //datagrid2.ItemsSource = fin_states;
        }



        public class fin_state
        {
            public string Transaction { get; set; }

            public double TotalAmount { get; set; }

            public string DataField { get; set; }
            public string Data { get; set; }
         }


        //public class fin
        //{
        //    public static string amt = "Ghc. 1000";
        //}

    }
}
