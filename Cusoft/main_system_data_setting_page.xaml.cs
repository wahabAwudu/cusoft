﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Cusoft.Models;
using Cusoft.Services;

namespace Cusoft
{
    /// <summary>
    /// Interaction logic for main_system_data_setting_page.xaml
    /// </summary>
    public partial class main_system_data_setting_page : Page
    {
        public main_system_data_setting_page()
        {
            InitializeComponent();
        }

        private void save_button_click(object sender, RoutedEventArgs e)
        {
            
            DataManipulationService save = new DataManipulationService();


            try
            {

                var data = new DataManipulation
                {

                    InterestRateForFullMembers = double.Parse(FullMembersInterestRate.Text),
                    InterestRateForWaitingMembers = double.Parse(WaitingMembersInterestRate.Text),
                    InterestRateForNonMembers = double.Parse(NonMembersInterestRate.Text),
                    DurationToAchieveFullMembership = int.Parse(MembershipDuration.Text),
                    AmountPerShare = double.Parse(PricePerShare.Text)

                };

                save.SaveDataManipulation(data);
                MessageBox.Show("Data Saved!");

                FullMembersInterestRate.Clear();
                NonMembersInterestRate.Clear();
                WaitingMembersInterestRate.Clear();
                MembershipDuration.Clear();
                PricePerShare.Clear();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Sorry, Data NOT Saved \n Try Again \n" + ex.Message);
            }


          
        }//end of method



        private void close_button_click(object sender, RoutedEventArgs e)
        {
            try
            {
                main_start_page acc = new main_start_page();
                NavigationService.Navigate(acc);
            }
            catch
            {

                MessageBox.Show("Sorry, can't reach page");
            }

        }//end method

       
        }
}
