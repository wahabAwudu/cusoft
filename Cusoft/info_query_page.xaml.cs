﻿using Cusoft.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cusoft
{
    /// <summary>
    /// Interaction logic for info_query_page.xaml
    /// </summary>
    public sealed partial class info_query_page : Page
    {
        

        public info_query_page()
        {
            InitializeComponent();

            //user_info_frame.Content = new graph_charts_page();
        }

        public class info
        {
          public string Id { get; set; }

          public string Name { get; set; }

          public string Acc_no { get; set; }
        }



        private void FullInfoSearchButtonClick(object sender, RoutedEventArgs e)
        {
            try
            {
                //instantiating the AccountService class
                AccountsServices accountServices = new AccountsServices();
                var account = accountServices.GetBankAccountByAccountStaffId(FullInfoSearchTextBox.Text);

                //instantiating the CustomerService class
                CustomerService customerService = new CustomerService();
                var customer = customerService.GetAllCustomerByStaffId(FullInfoSearchTextBox.Text);

                StaffIdTextblock.Text = customer.StaffId;
                NameTextblock.Text = customer.SurName + " " + customer.OtherNames;
                BalanceTextblock.Text = account.Balance.ToString();
                ContactTextblock.Text = customer.ContactNumber;
                EmailTextblock.Text = customer.EmailAddress;
                ResAddressTextblock.Text = customer.ResidentialAddress;
                DepartmentTextblock.Text = customer.Department;
                NetSalaryTextblock.Text = customer.NetIncome.ToString();

            } catch(Exception ex)
            {
                MessageBox.Show("StaffId has no Matches from Database\n" + ex.Message +"\n" + ex.StackTrace);
            }
           
           
        }


        private void Loan_button_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Properties["Name"] = NameTextblock.Text;
            Application.Current.Properties["StaffId"] = StaffIdTextblock.Text;
            Application.Current.Properties["ResAddress"] = ResAddressTextblock.Text;
            Application.Current.Properties["Contact"] = ContactTextblock.Text;
            Application.Current.Properties["Department"] = DepartmentTextblock.Text;
            Application.Current.Properties["NetSalary"] = NetSalaryTextblock.Text;


            CustomerService customer = new CustomerService();
            var customers = customer.GetAllCustomerByStaffId(StaffIdTextblock.Text);

            var dateCreated = customers.Created;
            var dateToday = DateTime.Now;

            int monthSoFar = GetMonthDifference(dateCreated, dateToday);

            if(monthSoFar >= 6)
            {
                NavigationService.Navigate(new full_member_page());
            }else
            {
                NavigationService.Navigate(new waiting_member_page());
            }
            
        }

        public int GetMonthDifference(DateTime startDate, DateTime endDate)
        {
            int monthsApart = 12 * (startDate.Year - endDate.Year) + startDate.Month - endDate.Month;
            return Math.Abs(monthsApart);
        }

        private void Update_button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Close_Acc_button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Disable_button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Enable_button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
