﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Cusoft
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //SignUpLoginWindow go = new SignUpLoginWindow();
            //go.Show();

            main.Content = new main_start_page();
        }



        //this is the main basic info of the app from the xml file
        private static string appname = ConfigurationManager.AppSettings["appName"];

        private static string theAuthor = ConfigurationManager.AppSettings["author"];

        private static string theReleaseDate = ConfigurationManager.AppSettings["releaseDate"];

        private static string theVersion = ConfigurationManager.AppSettings["appVersion"];


        private void main_acc_page_button(object sender, RoutedEventArgs e)
        {

            main.Content = new main_acc_page();
            main_acc_button.Background = Brushes.Black;
            main_acc_button.Foreground = Brushes.White;

            main_system_data_settings_button.Background = Brushes.Black;
            main_system_data_settings_button.Foreground = Brushes.MediumAquamarine;


            main_data_charts_button.Background = Brushes.Black;
            main_data_charts_button.Foreground = Brushes.MediumAquamarine;

            main_loan_button.Background = Brushes.Black;
            main_loan_button.Foreground = Brushes.MediumAquamarine;

            //main_fin_state_button.Background = Brushes.Black;
            //main_fin_state_button.Foreground = Brushes.MediumAquamarine;

            info_query_button.Background = Brushes.Black;
            info_query_button.Foreground = Brushes.MediumAquamarine;
        }



        private void info_query_page_button(object sender, RoutedEventArgs e)
        {
            main.Content = new info_query_page();
            info_query_button.Background = Brushes.Black;
            info_query_button.Foreground = Brushes.White;

            main_system_data_settings_button.Background = Brushes.Black;
            main_system_data_settings_button.Foreground = Brushes.MediumAquamarine;


            main_data_charts_button.Background = Brushes.Black;
            main_data_charts_button.Foreground = Brushes.MediumAquamarine;

            main_loan_button.Background = Brushes.Black;
            main_loan_button.Foreground = Brushes.MediumAquamarine;

            //main_fin_state_button.Background = Brushes.Black;
            //main_fin_state_button.Foreground = Brushes.MediumAquamarine;

            main_acc_button.Background = Brushes.Black;
            main_acc_button.Foreground = Brushes.MediumAquamarine;

        }




        //private void main_fin_state_page_button(object sender, RoutedEventArgs e)
        //{
        //    main.Content = new main_fin_state_page();
        //    main_fin_state_button.Background = Brushes.Black;
        //    main_fin_state_button.Foreground = Brushes.White;


        //    main_system_data_settings_button.Background = Brushes.Black;
        //    main_system_data_settings_button.Foreground = Brushes.MediumAquamarine;


        //    main_data_charts_button.Background = Brushes.Black;
        //    main_data_charts_button.Foreground = Brushes.MediumAquamarine;

        //    main_cashIn_out_button.Background = Brushes.Black;
        //    main_cashIn_out_button.Foreground = Brushes.MediumAquamarine;

        //    info_query_button.Background = Brushes.Black;
        //    info_query_button.Foreground = Brushes.MediumAquamarine;

        //    main_acc_button.Background = Brushes.Black;
        //    main_acc_button.Foreground = Brushes.MediumAquamarine;
        //}


        private void main_loan_page_button(object sender, RoutedEventArgs e)
        {
            main.Content = new main_loan_page();
            main_loan_button.Background = Brushes.Black;
            main_loan_button.Foreground = Brushes.White;

            main_system_data_settings_button.Background = Brushes.Black;
            main_system_data_settings_button.Foreground = Brushes.MediumAquamarine;

            main_data_charts_button.Background = Brushes.Black;
            main_data_charts_button.Foreground = Brushes.MediumAquamarine;

            //main_fin_state_button.Background = Brushes.Black;
            //main_fin_state_button.Foreground = Brushes.MediumAquamarine;

            info_query_button.Background = Brushes.Black;
            info_query_button.Foreground = Brushes.MediumAquamarine;

            main_acc_button.Background = Brushes.Black;
            main_acc_button.Foreground = Brushes.MediumAquamarine;
        }


        private void main_data_charts_page_button(object sender, RoutedEventArgs e)
        {
            main.Content = new main_data_charts_page();
            main_data_charts_button.Background = Brushes.Black;
            main_data_charts_button.Foreground = Brushes.White;


            main_system_data_settings_button.Background = Brushes.Black;
            main_system_data_settings_button.Foreground = Brushes.MediumAquamarine;

            main_loan_button.Background = Brushes.Black;
            main_loan_button.Foreground = Brushes.MediumAquamarine;

            //main_fin_state_button.Background = Brushes.Black;
            //main_fin_state_button.Foreground = Brushes.MediumAquamarine;

            info_query_button.Background = Brushes.Black;
            info_query_button.Foreground = Brushes.MediumAquamarine;

            main_acc_button.Background = Brushes.Black;
            main_acc_button.Foreground = Brushes.MediumAquamarine;
        }


        private void main_system_data_settings_page_button(object sender, RoutedEventArgs e)
        {
            main.Content = new main_system_data_setting_page();
            main_system_data_settings_button.Background = Brushes.Black;
            main_system_data_settings_button.Foreground = Brushes.White;


            main_data_charts_button.Background = Brushes.Black;
            main_data_charts_button.Foreground = Brushes.MediumAquamarine;

            main_loan_button.Background = Brushes.Black;
            main_loan_button.Foreground = Brushes.MediumAquamarine;

            //main_fin_state_button.Background = Brushes.Black;
            //main_fin_state_button.Foreground = Brushes.MediumAquamarine;

            info_query_button.Background = Brushes.Black;
            info_query_button.Foreground = Brushes.MediumAquamarine;

            main_acc_button.Background = Brushes.Black;
            main_acc_button.Foreground = Brushes.MediumAquamarine;
        }

        private void SignOutClick(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("You have successfully signed out\n Press 'OK' to Exit");
            Close();
        }


        //Border b = new Border();
        //b.BorderBrush = Brushes.AliceBlue;
        //b.BorderThickness = new Thickness(2);




        //private void button_click(object sender, RoutedEventArgs e)
        //{
        //    //this is the progress bar validation code. data binding modifications can be done.
        //    Duration dur = new Duration(TimeSpan.FromSeconds(1));

        //    DoubleAnimation doubleanimation = new DoubleAnimation(progress_bar.Value + 10, dur);

        //    progress_bar.BeginAnimation(ProgressBar.ValueProperty, doubleanimation);

        //    //MessageBox.Show("Are You Sure you want to Save New password?", "Password Reset Prompt");

        //    //btn1.IsEnabled = true;
        //    //secondWindow s = new secondWindow();
        //    //s.Show();
        //    //this.Close();
        //}
    }
}
