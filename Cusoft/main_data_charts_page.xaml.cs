﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cusoft
{
    /// <summary>
    /// Interaction logic for main_data_charts_page.xaml
    /// </summary>
    public partial class main_data_charts_page : Page
    {
        public main_data_charts_page()
        {
            InitializeComponent();
            reports_frame.Content = new main_fin_state_page();
        }

        //private void graph_chart_Click(object sender, RoutedEventArgs e)
        //{
        //    reports_frame.Content = new graph_charts_page();

        //    graph_chart.Background = Brushes.LightSeaGreen;
        //    graph_chart.Foreground = Brushes.White;

        //    fin_report.Foreground = Brushes.DodgerBlue;
        //    fin_report.Background = Brushes.White;
        //}

        private void fin_report_Click(object sender, RoutedEventArgs e)
        {
            reports_frame.Content = new main_fin_state_page();

            fin_report.Background = Brushes.LightSeaGreen;
            fin_report.Foreground = Brushes.White;

            //graph_chart.Foreground = Brushes.DodgerBlue;
            //graph_chart.Background = Brushes.White;
        }
    }
}
