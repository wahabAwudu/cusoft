﻿using Cusoft.Models;
using System.Linq;

namespace Cusoft.Services
{
    public class ReportsService
    {
        private readonly DataBaseContext _context;

        public ReportsService()
        {
            _context = new DataBaseContext();
        }


        public double GetTotalDeposits() => _context.Deposits.Sum(t => t.Amount);

        public double GetTotalWithdrawals() => _context.Withdrawals.Sum(t => t.Amount);

        public double GetTotalLoans() => _context.Loans.Sum(t => t.AmountApproved);

        public double GetTotalAccountBalance() => _context.Accounts.Sum(t => t.Balance);

        public Account GetAccountWithHighestSharePercentage() => _context.Accounts
            .OrderByDescending(t => t.SharesPercentage)
                .FirstOrDefault();

        public Account GetAccountWithLowestSharePercentage() => _context.Accounts
            .OrderByDescending(t => t.SharesPercentage)
                .LastOrDefault();

    }
}