﻿using Cusoft.Data;
using Cusoft.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cusoft.Services
{
    public class BankAccountsService
    {
        private readonly DataBaseContext _context;

        public BankAccountsService()
        {
            _context = new DataBaseContext();
        }


        //save bank account
        public void SaveBankAccount(Guid customerId, BankAccount bankAccount)
        {
            var customer = _context.Customers.FirstOrDefault(t => t.Id == customerId);
            customer?.BankAccounts.Add(bankAccount);
            _context.SaveChanges();


        }


        public IList<BankAccount> GetBankAccounts()
        {
            var bankAccounts = _context.BankAccounts.ToList();
            return bankAccounts;

        }


        //get bank account by id
        public BankAccount GetBankAccountById(Guid bankAccountId)
        {
            var bankAccount = _context.BankAccounts.FirstOrDefault(t => t.Id == bankAccountId);
            return bankAccount;

        }


        //get account by account number
        public BankAccount GetBankAccountByAccountNumber(string accountNumber)
        {
            var bankAccount = _context.BankAccounts.FirstOrDefault(t => t.AccountNumber == accountNumber);
            return bankAccount;

        }


        //making withdrawal
        public void MakeWithdrawal(Guid bankAccountId, WithDrawal withdrawal)
        {
            var bankAccount = _context.BankAccounts.FirstOrDefault(t => t.Id == bankAccountId);
            bankAccount.WithDrawals.Add(withdrawal);
            _context.SaveChanges();

            UpdateAccountBalance(bankAccount);

        }

        //making deposit
        public void MakeDeposit(Guid bankAccountId, Deposit deposit)
        {
            var bankAccount = _context.BankAccounts.FirstOrDefault(t => t.Id == bankAccountId);
            bankAccount.Deposits.Add(deposit);
            _context.SaveChanges();

            UpdateAccountBalance(bankAccount);

        }


        //updating the account balance
        private void UpdateAccountBalance(BankAccount bankAccount)
        {
            var withdrawals = bankAccount.WithDrawals.Sum(t => t.Amount);
            var deposits = bankAccount.Deposits.Sum(t => t.Amount);

            var dataManipulation = _context.DataManipulations.FirstOrDefault();
            bankAccount.Balance = (deposits - withdrawals) / dataManipulation.AmountPerShare;
            _context.SaveChanges();
        }


        //activate the account
        public void ActivateBankAccount(Guid bankAccountId)
        {
            var bankAccount = _context.BankAccounts.FirstOrDefault(t => t.Id == bankAccountId);
            bankAccount.AccountStatus = AccountStatus.Active;
            _context.SaveChanges();
        }


        //deactivate the account
        public void DeactivateBankAccount(Guid bankAccountId)
        {
            var bankAccount = _context.BankAccounts.FirstOrDefault(t => t.Id == bankAccountId);
            bankAccount.AccountStatus = AccountStatus.InActive;
            _context.SaveChanges();
        }
    }
}
