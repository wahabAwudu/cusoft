﻿using Cusoft.Models;
using System;
using System.Linq;

namespace Cusoft.Services
{
    public class LoanService
    {
        private readonly DataBaseContext _context;

        public LoanService()
        {
            _context = new DataBaseContext();
        }



        public void SaveLoan(string customerStaffId, Loan loan)
        {
            var bankAccount = _context.Accounts.FirstOrDefault(t => t.staffId == customerStaffId);
            var dataManipulation = _context.DataManipulations.FirstOrDefault();

            switch (MemberLevelShip(customerStaffId))
            {
                case MemberLevel.FullMember:
                    {
                        loan.InterestAmount = loan.AmountApproved *
                            dataManipulation.InterestRateForFullMembers * loan.DurationInMonths / 100;
                        break;
                    }

                case MemberLevel.WaitingMember:
                    {
                        loan.InterestAmount = loan.AmountApproved *
                            dataManipulation.InterestRateForWaitingMembers * loan.DurationInMonths / 100;
                        break;
                    }


                case MemberLevel.NonMember:
                    {
                        loan.InterestAmount = loan.AmountApproved *
                            dataManipulation.InterestRateForNonMembers * loan.DurationInMonths / 100;
                        break;
                    }
            }

            bankAccount.Loans.Add(loan);
            _context.SaveChanges();
        }


        private MemberLevel MemberLevelShip(string customerStaffId)
        {
            if (customerStaffId == null)
            {
                return MemberLevel.NonMember;
            }
            else
            {
                var sixMounthsTime = _context.Customers.FirstOrDefault(t => t.StaffId == customerStaffId)
                .Created.AddMonths(6);

                return (DateTime.Now > sixMounthsTime) ? MemberLevel.FullMember : MemberLevel.WaitingMember;
            }

        }

    }

    public enum MemberLevel
    {
        FullMember,
        WaitingMember,
        NonMember
    }
}
