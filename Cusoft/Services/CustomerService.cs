﻿using Cusoft.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Cusoft.Services
{
    public class CustomerService
    {
        private readonly DataBaseContext _context;

        public CustomerService()
        {
            _context = new DataBaseContext();
        }


        public IEnumerable<Customer> GetAllCustomers()
        {
            var customers = _context.Customers.Include(t => t.Accounts).ToList();
            return customers;
        }



        public Customer GetAllCustomerByStaffId (string staffId)
        {
            var customer = _context.Customers
                .Include(t => t.Accounts)
                .FirstOrDefault(t => t.StaffId == staffId);
            return customer;
        }



        public void AddCustomer(Customer customer)
        {
            _context.Customers.Add(customer);
            _context.Accounts.Add(new Account
            {
                staffId = customer.StaffId
            });
            _context.SaveChanges();


        }



        public void DeleteCustomer(string staffId)
        {
            var customer = GetAllCustomerByStaffId(staffId);
            _context.Customers.Remove(customer);
            _context.SaveChanges();
        }



        public void UpdateCustomer(string staffId, Customer customer)
        {
            var originalCustomer = GetAllCustomerByStaffId(staffId);

            originalCustomer.SurName = customer.SurName;
            originalCustomer.OtherNames = customer.OtherNames;
            originalCustomer.Gender = customer.Gender;

            originalCustomer.DateOfBirth = customer.DateOfBirth;
            originalCustomer.JobTittle = customer.JobTittle;

            originalCustomer.StaffId = customer.StaffId;
            originalCustomer.SsnitId = customer.SsnitId;
            originalCustomer.Department = customer.Department;

            originalCustomer.ContactNumber = customer.ContactNumber;
            originalCustomer.EmailAddress = customer.EmailAddress;
            originalCustomer.ResidentialAddress = customer.ResidentialAddress;

            originalCustomer.FirstBeneficiaryContact = customer.FirstBeneficiaryContact;
            originalCustomer.FirstBeneficiaryName = customer.FirstBeneficiaryName;

            originalCustomer.SecondBeneficiaryName = customer.SecondBeneficiaryName;
            originalCustomer.SecondBeneficiaryContact = customer.SecondBeneficiaryContact;

            originalCustomer.ThirdBeneficiaryName = customer.ThirdBeneficiaryName;
            originalCustomer.ThirdBeneficiaryContact = customer.ThirdBeneficiaryContact;

            originalCustomer.NextOfKinName = customer.NextOfKinName;
            originalCustomer.NextOfKinContact = customer.NextOfKinContact;

            _context.SaveChanges();
        }

    }
}
