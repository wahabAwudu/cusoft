﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Cusoft.Models;

namespace Cusoft.Services
{
    class CustomerServices
    {
        create_acc_page Invoke = new create_acc_page();

        //adding new information to the customer table
        public void AddToCustomersTable()
        {

            using ( DataBaseContext context = new DataBaseContext() )
            {
                CustomerService call = new CustomerService();

                string CustomGender = " ";

                //setting customer gender with condition
                if( Invoke.MaleGenderOption.Text == "MALE")
                {
                    CustomGender = "Male";
                }
                else 
                if(Invoke.FemaleGenderOption.Text == "FEMALE")
                {
                    CustomGender = "Female";
                }

                decimal NetSalary = 0;
                decimal.TryParse(Invoke.NetSalaryTextBox.Text, out NetSalary);
            
                context.Customers.Add(new Customer
                {
                    OtherNames = Invoke.OtherNamesTextBox.Text,
                    SurName = Invoke.SurNameTextBox.Text,
                    Gender = CustomGender,
                    //ProfileImage = Invoke.imgPhoto.
                    DateOfBirth = Invoke.DOB.SelectedDate,
                    JobTittle = Invoke.JobTitleTextBox.Text,
                    StaffId = Invoke.StaffIdTextBox.Text,
                    SsnitId = Invoke.SsnitIdTextBox.Text,

                    NetIncome = NetSalary,
                    Department = Invoke.DepartmentTextBox.Text,

                    ContactNumber = Invoke.PhoneTextBox.Text,
                    EmailAddress = Invoke.Email_textBox.Text,
                    ResidentialAddress = Invoke.Residencial_textBox.Text,

                    FirstBeneficiaryName = Invoke.FirstBeneficiaryName.Text,
                    FirstBeneficiaryContact = Invoke.FirstBeneficiaryContact.Text,

                    SecondBeneficiaryName = Invoke.SecondBeneficiaryName.Text,
                    SecondBeneficiaryContact = Invoke.SecondBeneficiaryContact.Text,

                    ThirdBeneficiaryName = Invoke.ThirdBeneficiaryName.Text,
                    ThirdBeneficiaryContact = Invoke.ThirdBeneficiaryContact.Text,

                    NextOfKinName = Invoke.nok_name_textBox.Text,
                    NextOfKinContact = Invoke.nok_contact_textBox.Text

                   });

                context.SaveChanges();
            }
        } //end of method

    }
}
