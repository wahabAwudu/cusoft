﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cusoft.Models;

namespace Cusoft.Services
{
    class AccountsServices
    {
       private readonly DataBaseContext _context;

        public AccountsServices()
        {
            _context = new DataBaseContext();
        }


        //save bank account
        public void SaveBankAccount(int customerId, Account servicesAccount)
        {
            var customer = _context.Customers.FirstOrDefault(t => t.Id == customerId);

            customer?.Accounts.Add(servicesAccount);
            _context.SaveChanges();
        }



        public IList<Account> GetBankAccounts()
        {
            var bankAccounts = _context.Accounts.ToList();
            return bankAccounts;

        }


        //get bank account by id
        public Account GetBankAccountByStaffId(string accountStaffId)
        {
            var bankAccount = _context.Accounts.FirstOrDefault(t => t.staffId == accountStaffId);
            return bankAccount;

        }


        //get account by staff id
        public Account GetBankAccountByAccountStaffId(string accountStaffId)
        {
            var bankAccount = _context.Accounts.FirstOrDefault(t => t.staffId == accountStaffId);
            return bankAccount;

        }

        // used to get account balance
        public double GetAccountBalance(string accountStaffId)
        {
            var bankAccount = _context.Accounts.FirstOrDefault(t => t.staffId == accountStaffId);
            return bankAccount.Balance;
        }

        //making withdrawal
        public void MakeWithdrawal(string accountStaffId, WithDrawal withdrawal)
        {
            var bankAccount = _context.Accounts.FirstOrDefault(t => t.staffId == accountStaffId);

            bankAccount.WithDrawals.Add(withdrawal);
            _context.SaveChanges();

            UpdateAccountBalance(bankAccount);

        }

        //checking withdrawal staffId before withdrawing
        public bool WithdrawalCredCheck(string withdrawalStaffId)
        {
            return _context.Accounts.Any(t => t.staffId == withdrawalStaffId) ? true : false;
        }



        //making deposit
        public void MakeDeposit(string accountStaffId, Deposit deposit)
        {
            var bankAccount = _context.Accounts.FirstOrDefault(t => t.staffId == accountStaffId);

            bankAccount.Deposits.Add(deposit);
            _context.SaveChanges();

            UpdateAccountBalance(bankAccount);

        }


        //checking deposit staffId before depositing
        public bool DepositCredCheck(string depositStaffId)
        {
            return _context.Accounts.Any(t => t.staffId == depositStaffId)? true : false;
        }


        //updating the account balance and it is not to be invoked manually
        private void UpdateAccountBalance(Account bankAccount)
        {
            var withdrawals = bankAccount.WithDrawals.Sum(t => t.Amount);
            var deposits = bankAccount.Deposits.Sum(t => t.Amount);

            var dataManipulation = _context.DataManipulations.FirstOrDefault();
            bankAccount.Balance = deposits - withdrawals;

            bankAccount.Shares = bankAccount.Balance / dataManipulation.AmountPerShare;

           var totalShares = _context.Accounts.Sum(t => t.Shares);

            if ((totalShares != 0))
            {
                bankAccount.SharesPercentage = bankAccount.Shares / totalShares * 100;
            }

            //bankAccount.Balance = (deposits - withdrawals) / dataManipulation.AmountPerShare;
            _context.SaveChanges();
        }


        //activate the account
        public void ActivateBankAccount(int bankAccountId)
        {
            var bankAccount = _context.Accounts.FirstOrDefault(t => t.Id == bankAccountId);
            bankAccount.AccountStatus = AccountStatus.Active;
            _context.SaveChanges();
        }


        //deactivate the account
        public void DeactivateBankAccount(int bankAccountId)
        {
            var bankAccount = _context.Accounts.FirstOrDefault(t => t.Id == bankAccountId);
            bankAccount.AccountStatus = AccountStatus.InActive;
            _context.SaveChanges();
        }
    }
}
