﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cusoft.Models;

namespace Cusoft.Services
{
    public class DataManipulationServices
    {
        main_system_data_setting_page Invoke = new main_system_data_setting_page();


        public void AddToDataManipulationTable()
        {
            using (DataBaseContext ctx = new DataBaseContext()) 
            {

                double FullMembersInterestRate = 0;
                double WaitingMembersInterestRate = 0;
                double NonMembersInterestRate = 0;
                int DurationToFullMembership = 0;
                double PricePerShare = 0;

                double.TryParse(Invoke.FullMembersInterestRate.Text, out FullMembersInterestRate);
                double.TryParse(Invoke.WaitingMembersInterestRate.Text, out WaitingMembersInterestRate);
                double.TryParse(Invoke.NonMembersInterestRate.Text, out NonMembersInterestRate);
                double.TryParse(Invoke.PricePerShare.Text, out PricePerShare);

                int.TryParse(Invoke.MembershipDuration.Text, out DurationToFullMembership);



                ctx.DataManipulations.Add(
                    new DataManipulation
                    {
                        InterestRateForFullMembers = FullMembersInterestRate,
                        InterestRateForWaitingMembers = WaitingMembersInterestRate,
                        InterestRateForNonMembers = NonMembersInterestRate,
                        AmountPerShare = PricePerShare,
                        DurationToAchieveFullMembership = DurationToFullMembership

                    });

                ctx.SaveChanges();
            }
        } //end method
       
    }
}
