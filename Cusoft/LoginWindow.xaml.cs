﻿using Cusoft.Models;
using Cusoft.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Cusoft
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void SignUpClick(object sender, RoutedEventArgs e)
        {
            SignUpWindow go = new SignUpWindow();
            go.Show();
            this.Close();
        }


        private void LoginButtonClick(object sender, RoutedEventArgs e)
        {
            UserService use = new UserService();

            try
            {
                var login = new User {
                    UserName = UserNameTextbox.Text,
                    Password = PassWordbox.Password.ToString()
                };

                if( (use.Login(login)) != true)
                {
                    MessageBox.Show("Login Credentials Wrong!\n Try Again or Create New Account");
                }
                else
                {
                    use.Login(login);

                    MainWindow go = new MainWindow();
                    go.Show();
                    Close();
                }
                

            } catch(Exception ex)
            {
                MessageBox.Show("Snap! Something went Wrong\n Please Try Again \n" + ex.Message);
            }

            
        }
    }
}
