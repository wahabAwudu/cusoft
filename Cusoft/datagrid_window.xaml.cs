﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Cusoft
{
    /// <summary>
    /// Interaction logic for datagrid_window.xaml
    /// </summary>
    public partial class datagrid_window : Window
    {
        public datagrid_window()
        {
            InitializeComponent();

            List<info> infos = new List<info>();

            infos.Add(new info() { Id= 1, Name = "abdul wahab", Acc_no= "224455" });
            infos.Add(new info() { Id= 2, Name = "me", Acc_no = "4545655" });
            datagrid.ItemsSource = infos; 
        }

        public class info
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Acc_no { get; set; }
        }

        private void mouse_double_click(object sender, RoutedEventArgs e)
        {
            full_member_page full = new full_member_page();

            this.Close();
           
        }
    }
}
