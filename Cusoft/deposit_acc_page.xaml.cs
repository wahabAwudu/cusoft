﻿using Cusoft.Models;
using Cusoft.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cusoft
{
    /// <summary>
    /// Interaction logic for deposit_acc_page.xaml
    /// </summary>
    public partial class deposit_acc_page : Page
    {
        public deposit_acc_page()
        {
            InitializeComponent();
        }

        private void depositCancelButtonClick(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new main_start_page());
        }



        private void depositSaveButtonClick(object sender, RoutedEventArgs e)
        {
            AccountsServices accountsServices = new AccountsServices();


            try
            {
                if (accountsServices.DepositCredCheck(StaffIdTextbox.Text))
                {
                    var deposit = new Deposit
                    {
                        staffId = StaffIdTextbox.Text,
                        Amount = double.Parse(Amounttextbox.Text)
                    };

                    accountsServices.MakeDeposit(deposit.staffId, deposit);
                    MessageBox.Show("Deposit Saved Successfully");

                    StaffIdTextbox.Clear();
                    Amounttextbox.Clear();
                }
                else
                {
                    MessageBox.Show("The StaffId has no Database Matches");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Deposit cant be made\n Try Again\n" + ex.Message + "\n" + ex.StackTrace);
            }

        }
    }
}
