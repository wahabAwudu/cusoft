﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cusoft
{
    /// <summary>
    /// Interaction logic for waiting_member_page.xaml
    /// </summary>
    public partial class waiting_member_page : Page
    {
        public waiting_member_page()
        {
            InitializeComponent();
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            nameTextblock.Text = Application.Current.Properties["Name"].ToString();
            staffIdTextblock.Text = Application.Current.Properties["StaffId"].ToString();
            ResAddressTextblock.Text = Application.Current.Properties["ResAddress"].ToString();
            ContactTextblock.Text = Application.Current.Properties["Contact"].ToString();
            DepartmentTextblock.Text = Application.Current.Properties["Department"].ToString();
            NetSalaryTextblock.Text = Application.Current.Properties["NetSalary"].ToString();


        }

        private void save_button_click(object sender, RoutedEventArgs e)
        {

        }


        private void cancel_button_click(object sender, RoutedEventArgs e)
        {

        }


        private void choose_guarantor_bnt(object sender, RoutedEventArgs e)
        {
            datagrid_window show = new datagrid_window();
            show.Show();
        }

        
    }
}
