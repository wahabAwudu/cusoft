﻿using Cusoft.Models;
using Cusoft.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cusoft
{
    /// <summary>
    /// Interaction logic for withdrawal_acc_page.xaml
    /// </summary>
    public partial class withdrawal_acc_page : Page
    {
        public withdrawal_acc_page()
        {
            InitializeComponent();
        }

        

        private void withdrawalCancelButtonClick(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new main_start_page());
        }

        private void withdrawalSaveButtonClick(object sender, RoutedEventArgs e)
        {
            AccountsServices accountsServices = new AccountsServices();


            try
            {
                if (accountsServices.WithdrawalCredCheck(StaffIdTextbox.Text))
                {
                    var withdraw = new WithDrawal
                    {
                        StaffId = StaffIdTextbox.Text,
                        Amount = double.Parse(Amounttextbox.Text)
                    };

                    if(accountsServices.GetAccountBalance(StaffIdTextbox.Text) >= double.Parse(Amounttextbox.Text) )
                    {
                        accountsServices.MakeWithdrawal(withdraw.StaffId, withdraw);
                        MessageBox.Show("WithDrawal Made Successfully");

                        StaffIdTextbox.Clear();
                        Amounttextbox.Clear();
                    }
                    else
                    {
                        MessageBox.Show("You dont have sufficient balance");
                    }

                    
                }
                else
                {
                    MessageBox.Show("The StaffId has no Database Matches");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Deposit cant be made\n Try Again\n" + ex.Message + "\n" + ex.StackTrace);
            }

        }
    }
}
