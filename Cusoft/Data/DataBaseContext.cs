﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Cusoft.Models
{
   public class DataBaseContext : DbContext
    {
        public DataBaseContext() :base("CuSoft DB") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Customer>().Property(p => p.ProfileImage).HasColumnType("image");
        }


       public DbSet<Customer> Customers { get; set; }
       public DbSet<Account> Accounts { get; set; }
       public DbSet<Deposit> Deposits { get; set; }
       public DbSet<WithDrawal> Withdrawals { get; set; }
       public DbSet<Loan> Loans { get; set; }

       public DbSet<DataManipulation> DataManipulations { get; set; }
       public DbSet<User> Users { get; set; }
       public DbSet<NonMemberLoan> NonMemberLoans{ get; set; }
        
    }
}
