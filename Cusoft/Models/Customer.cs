﻿//using Cusoft.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace Cusoft.Models
{
    public class Customer
    {

        public Customer()
        {
            Created = DateTime.Now;
            Accounts = new List<Account>();
        }

        public int Id { get; set; }

        public byte [] ProfileImage { get; set; }

        [MaxLength(256)]
        public string SurName { get; set; }

        [MaxLength(256)]
        public string OtherNames { get; set; }       


        public string Gender { get; set; }


        public DateTime? DateOfBirth { get; set; }

        [MaxLength(256)]
        public string JobTittle { get; set; }

        [MaxLength(256)]
        public string StaffId { get; set; }


        [MaxLength(256)]
        public string SsnitId { get; set; }

       
        public decimal NetIncome { get; set; }

        //[MaxLength(256)]
        //public string Faculty { get; set; }

        [MaxLength(256)]
        public string Department { get; set; }

        //[MaxLength(256)]
        //public string WorkShift { get; set; }

        [MaxLength(50)]
        public string ContactNumber { get; set; }

        [MaxLength(256)]
        public string EmailAddress { get; set; }

        [MaxLength(256)]
        public string ResidentialAddress { get; set; }


        [MaxLength(256)]
        public string FirstBeneficiaryName { get; set; }

        [MaxLength(256)]
        public string FirstBeneficiaryContact { get; set; }


        [MaxLength(256)]
        public string SecondBeneficiaryName { get; set; }

        [MaxLength(256)]
        public string SecondBeneficiaryContact { get; set; }


        [MaxLength(256)]
        public string ThirdBeneficiaryName { get; set; }

        [MaxLength(256)]
        public string ThirdBeneficiaryContact { get; set; }


        [MaxLength(256)]
        public string NextOfKinName { get; set; }

        [MaxLength(256)]
        public string NextOfKinContact { get; set; }

        public DateTime Created { get; set; }

        

    public ICollection<Account> Accounts { get; set; }


    }


   


    //public class Anything
    //{
    //    private readonly DataBaseContext _ctx;

    //    public Anything()
    //    {
    //        _ctx = new DataBaseContext();
    //    }
        
    //    public Customer GetAllCustomers()
    //    {
    //        var customers = _ctx.Customers.Where(c => c.FirstName.StartsWith("y")).FirstOrDefault();
    //        return customers;
    //    }
        

    //}
}
