﻿using System;
using System.Collections.Generic;

namespace Cusoft.Models
{
    public class Account
    {
        public Account()
        {
            
            Created = DateTime.UtcNow;
            Deposits = new List<Deposit>();
            WithDrawals = new List<WithDrawal>();
            Loans = new List<Loan>();
        }
        public int Id { get; set; }
        public string staffId { get; set; }
        public double Balance { get; set; }
        public double Shares { get; set; }
        public double SharesPercentage { get; set; }
        public AccountStatus AccountStatus { get; set; } = AccountStatus.Active;
        public ICollection<Deposit> Deposits { get; set; }
        public ICollection<WithDrawal> WithDrawals { get; set; }
        public ICollection<Loan> Loans { get; set; }

        public DateTime Created { get; set; }
    }

    public enum AccountStatus
    {
        Active = 1,
        InActive = 2
    }
}
