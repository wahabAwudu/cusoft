﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cusoft.Models
{

    public class WithDrawal
    {
        public WithDrawal()
        {
           
            WithDrawalDate = DateTime.Now;
        }
        
        
        public int Id { get; set; }
        public string StaffId { get; set; }
        public double Amount { get; set; }
        public DateTime WithDrawalDate { get; set; }
    }
}
