﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cusoft.Models
{
    public class Loan
    {
        public Loan() 
        {
            Created = DateTime.Now;
        }

        public int Id { get; set; }

        [MaxLength(256)]
        public string StaffId { get; set; }
       
        public double AmountApplying { get; set; }
        public double AmountApproved { get; set; }
        public int DurationInMonths { get; set; }
        public double AmountPaying { get; set; }
        public double InterestAmount { get; set; }

        public DateTime Created { get; set; }

        //[MaxLength(256)]
        //public string GuarantorId { get; set; }

        //[MaxLength(256)]
        //public string GuarantorName { get; set; }

        //[MaxLength(256)]
        //public string GuarantorContact { get; set; }

        //[MaxLength(256)]
        //public string GuarantorAddress { get; set; }




    }
}
