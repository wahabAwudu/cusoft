﻿using Cusoft.Models;
using Cusoft.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Cusoft
{
    /// <summary>
    /// Interaction logic for SignUpLoginWindow.xaml
    /// </summary>
    public partial class SignUpWindow : Window
    {
        public SignUpWindow()
        {
            InitializeComponent();
           
        }

        private void SignUpButtonClick(object sender, RoutedEventArgs e)
        {

            UserService use = new UserService();

            try
            {
                var user = new User {
                    UserName = UserNameTextbox.Text,
                    Password = PassWordbox.Password.ToString()
                };

                
                    use.Register(user);
                    MessageBox.Show("Sign Up Success!!\n Now You Can Login");

                    LoginWindow go = new LoginWindow();
                    go.Show();
                    Close();
                
                
            }catch
            {
                MessageBox.Show("Snap! Something went wrong\n Please Retry");
            }

        }//end of method



        private void LoginClick(object sender, RoutedEventArgs e)
        {
            LoginWindow go = new LoginWindow();
            go.Show();
            Close();
        }
    }
}
