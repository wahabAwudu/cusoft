namespace Cusoft.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAll : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Account",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        staffId = c.String(),
                        Balance = c.Double(nullable: false),
                        Shares = c.Double(nullable: false),
                        SharesPercentage = c.Double(nullable: false),
                        AccountStatus = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false),
                        Customer_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customer", t => t.Customer_Id)
                .Index(t => t.Customer_Id);
            
            CreateTable(
                "dbo.Deposit",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        staffId = c.String(),
                        Amount = c.Double(nullable: false),
                        DepositDate = c.DateTime(nullable: false),
                        Account_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Account", t => t.Account_Id)
                .Index(t => t.Account_Id);
            
            CreateTable(
                "dbo.Loan",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StaffId = c.String(maxLength: 256),
                        AmountApplying = c.Double(nullable: false),
                        AmountApproved = c.Double(nullable: false),
                        DurationInMonths = c.Int(nullable: false),
                        AmountPaying = c.Double(nullable: false),
                        InterestAmount = c.Double(nullable: false),
                        Created = c.DateTime(nullable: false),
                        Account_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Account", t => t.Account_Id)
                .Index(t => t.Account_Id);
            
            CreateTable(
                "dbo.WithDrawal",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StaffId = c.String(),
                        Amount = c.Double(nullable: false),
                        WithDrawalDate = c.DateTime(nullable: false),
                        Account_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Account", t => t.Account_Id)
                .Index(t => t.Account_Id);
            
            CreateTable(
                "dbo.Customer",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProfileImage = c.Binary(storeType: "image"),
                        SurName = c.String(maxLength: 256),
                        OtherNames = c.String(maxLength: 256),
                        Gender = c.String(),
                        DateOfBirth = c.DateTime(),
                        JobTittle = c.String(maxLength: 256),
                        StaffId = c.String(maxLength: 256),
                        SsnitId = c.String(maxLength: 256),
                        NetIncome = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Department = c.String(maxLength: 256),
                        ContactNumber = c.String(maxLength: 50),
                        EmailAddress = c.String(maxLength: 256),
                        ResidentialAddress = c.String(maxLength: 256),
                        FirstBeneficiaryName = c.String(maxLength: 256),
                        FirstBeneficiaryContact = c.String(maxLength: 256),
                        SecondBeneficiaryName = c.String(maxLength: 256),
                        SecondBeneficiaryContact = c.String(maxLength: 256),
                        ThirdBeneficiaryName = c.String(maxLength: 256),
                        ThirdBeneficiaryContact = c.String(maxLength: 256),
                        NextOfKinName = c.String(maxLength: 256),
                        NextOfKinContact = c.String(maxLength: 256),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DataManipulation",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InterestRateForFullMembers = c.Double(nullable: false),
                        InterestRateForNonMembers = c.Double(nullable: false),
                        InterestRateForWaitingMembers = c.Double(nullable: false),
                        AmountPerShare = c.Double(nullable: false),
                        DurationToAchieveFullMembership = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NonMemberLoan",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StaffId = c.String(maxLength: 256),
                        AmountApplying = c.Double(nullable: false),
                        AmountApproved = c.Double(nullable: false),
                        DurationInMonths = c.Int(nullable: false),
                        AmountPaying = c.Double(nullable: false),
                        InterestAmount = c.Double(nullable: false),
                        GuarantorId = c.String(maxLength: 256),
                        GuarantorName = c.String(maxLength: 256),
                        GuarantorContact = c.String(maxLength: 256),
                        GuarantorAddress = c.String(maxLength: 256),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserName = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Account", "Customer_Id", "dbo.Customer");
            DropForeignKey("dbo.WithDrawal", "Account_Id", "dbo.Account");
            DropForeignKey("dbo.Loan", "Account_Id", "dbo.Account");
            DropForeignKey("dbo.Deposit", "Account_Id", "dbo.Account");
            DropIndex("dbo.WithDrawal", new[] { "Account_Id" });
            DropIndex("dbo.Loan", new[] { "Account_Id" });
            DropIndex("dbo.Deposit", new[] { "Account_Id" });
            DropIndex("dbo.Account", new[] { "Customer_Id" });
            DropTable("dbo.User");
            DropTable("dbo.NonMemberLoan");
            DropTable("dbo.DataManipulation");
            DropTable("dbo.Customer");
            DropTable("dbo.WithDrawal");
            DropTable("dbo.Loan");
            DropTable("dbo.Deposit");
            DropTable("dbo.Account");
        }
    }
}
