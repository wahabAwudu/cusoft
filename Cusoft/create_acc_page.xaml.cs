﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Cusoft.Services;
using Microsoft.Win32;
using Cusoft.Models;

namespace Cusoft
{
    /// <summary>
    /// Interaction logic for create_acc_page.xaml
    /// </summary>
    public partial class create_acc_page : Page
    {
        public create_acc_page()
        {
            InitializeComponent();
        }



        private void create_acc_page_cancel_button_click(object sender, RoutedEventArgs e)
        {
            main_start_page nav = new main_start_page();
            NavigationService.Navigate(nav);
       }
        

        private void create_acc_page_save_button_click(object sender, RoutedEventArgs e)
        {
            AccountsServices accountsservices = new AccountsServices();

            CustomerService customerService = new CustomerService();

            try
            {
                var customer = new Customer
                {
                    
                    OtherNames = OtherNamesTextBox.Text,
                    SurName = SurNameTextBox.Text,

                    Gender = GenderCombobox.SelectedValue.ToString(),
                    DateOfBirth = DateTime.Parse(DOB.Text),
                    JobTittle = JobTitleTextBox.Text,
                    StaffId = StaffIdTextBox.Text,
                    SsnitId = SsnitIdTextBox.Text,

                    NetIncome = decimal.Parse(NetSalaryTextBox.Text),
                    Department = DepartmentTextBox.Text,

                    ContactNumber = PhoneTextBox.Text,
                    EmailAddress = Email_textBox.Text,
                    ResidentialAddress = Residencial_textBox.Text,

                    FirstBeneficiaryName = FirstBeneficiaryName.Text,
                    FirstBeneficiaryContact = FirstBeneficiaryContact.Text,

                    SecondBeneficiaryName = SecondBeneficiaryName.Text,
                    SecondBeneficiaryContact = SecondBeneficiaryContact.Text,

                    ThirdBeneficiaryName = ThirdBeneficiaryName.Text,
                    ThirdBeneficiaryContact = ThirdBeneficiaryContact.Text,

                    NextOfKinName = nok_name_textBox.Text,
                    NextOfKinContact = nok_contact_textBox.Text
                };

                customerService.AddCustomer(customer);
                MessageBox.Show("Account Created Successfully!");

                

                ClearAllFields();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Can't be save!! \n Try Entering the Data again \n" + ex.StackTrace + "\n" + ex.Message);
            }
            

            /*when data not entered, it is blur when all required fields entered, it is shown
            when pressed, it pops up asking to save ( sure? ). if yes, continue. if no, the popup closes and stays so*/
        }


        
        public void ClearAllFields()
        {
            SurNameTextBox.Clear();
            OtherNamesTextBox.Clear();

            GenderCombobox.Text = null;
            //imgPhoto.Source = null;

            DOB.SelectedDate = null;
            JobTitleTextBox.Clear();

            StaffIdTextBox.Clear();
            SsnitIdTextBox.Clear();
            NetSalaryTextBox.Clear();
            DepartmentTextBox.Clear();

            PhoneTextBox.Clear();
            Email_textBox.Clear();
            Residencial_textBox.Clear();

            FirstBeneficiaryContact.Clear();
            FirstBeneficiaryName.Clear();

            SecondBeneficiaryContact.Clear();
            SecondBeneficiaryName.Clear();

            ThirdBeneficiaryContact.Clear();
            ThirdBeneficiaryName.Clear();

            nok_contact_textBox.Clear();
            nok_name_textBox.Clear();
        }





        //private void browse_for_image_button_click(object sender, RoutedEventArgs e)
        //{

        //    OpenFileDialog open = new OpenFileDialog();
        //    open.Title = "Select a Picture";
        //    open.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
        //      "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
        //      "Portable Network Graphic (*.png)|*.png";

        //    if(open.ShowDialog() == true)
        //    {
        //        imgPhoto.Source = new BitmapImage(new Uri(open.FileName));
        //    }

        //}


        /*look out to plan an algorithm for generating the account number

        private void create_acc_page_edit_button_click(object sender, RoutedEventArgs e)
        {
            when nothing is saved at at drafts yet, it is blur. show when a draft work is resumed
        }
        */
    }
}
