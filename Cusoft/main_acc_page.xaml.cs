﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cusoft
{
    /// <summary>
    /// Interaction logic for main_acc_page.xaml
    /// </summary>
    public partial class main_acc_page : Page
    {
        public main_acc_page()
        {
            InitializeComponent();
            innerPageNavSection.Content = new create_acc_page();
        }


        private void create_acc_section_main_button_click (object sender, RoutedEventArgs e)
        {
            innerPageNavSection.Content = new create_acc_page();

            create_acc_section_main_button.Background = Brushes.LightSeaGreen;
            create_acc_section_main_button.Foreground = Brushes.White;

            withdrawal_section_main_button.Foreground = Brushes.DodgerBlue;
            withdrawal_section_main_button.Background = Brushes.White;

            deposit_section_main_button.Foreground = Brushes.DodgerBlue;
            deposit_section_main_button.Background = Brushes.White;

           
        }



        private void deposit_section_main_button_click(object sender, RoutedEventArgs e)
        {
            innerPageNavSection.Content = new deposit_acc_page();


            deposit_section_main_button.Foreground = Brushes.White;
            deposit_section_main_button.Background = Brushes.LightSeaGreen;

            create_acc_section_main_button.Background = Brushes.White;
            create_acc_section_main_button.Foreground = Brushes.DodgerBlue;

            withdrawal_section_main_button.Foreground = Brushes.DodgerBlue;
            withdrawal_section_main_button.Background = Brushes.White;

           
        }



        private void withdrawal_section_main_button_click(object sender, RoutedEventArgs e)
        {
            innerPageNavSection.Content = new withdrawal_acc_page();


            withdrawal_section_main_button.Foreground = Brushes.White;
            withdrawal_section_main_button.Background = Brushes.LightSeaGreen;

            create_acc_section_main_button.Background = Brushes.White;
            create_acc_section_main_button.Foreground = Brushes.DodgerBlue;

            
            deposit_section_main_button.Foreground = Brushes.DodgerBlue;
            deposit_section_main_button.Background = Brushes.White;

           

        }


    }
}
